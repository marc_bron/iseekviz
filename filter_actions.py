"""Module provides ways of filtering and binning/summarizing actions from user
action sequences.
Author: Marc Bron
Date: 16-01-2015
"""


class ActionSequence(object):
    """ActionSequence is the container for a user's action sequence."""

    def __init__(self, sequence):
        """Take comma separated string of sequences, store as list of
        actions.
        """
        self.sequence = sequence.split(',')

    def bin_repetitions(self, bin_size, action_whitelist=[],
                        action_blacklist=[]):
        """Summarize a repeating sequence as a single action. Note: abusing
        bin_size smaller than 1 to filter actions.
        >>> actseq = ActionSequence('a,b,a,a,a,a,b,b,b,a')

        A bin_size of 1 should return the sequence
        >>> actseq.bin_repetitions(1, ['a', 'b'])
        ['a_1', 'b_1', 'a_1', 'a_1', 'a_1', 'a_1', 'b_1', 'b_1', 'b_1', 'a_1']

        This should just return bins for b
        >>> actseq.bin_repetitions(2, ['b'])
        ['a', 'b', 'a', 'a', 'a', 'a', 'b_2', 'b', 'a']

        This should return bins for a and b
        >>> actseq.bin_repetitions(2, ['a', 'b'])
        ['a', 'b', 'a_2', 'a_2', 'b_2', 'b', 'a']
        """
        counts = dict(zip(
            action_whitelist,
            [0 for _ in range(len(action_whitelist))]))
        binned_seq = []
        seq_len = len(self.sequence)
        for i, action in enumerate(self.sequence):
            if action in action_blacklist:
                continue
            if action in action_whitelist:
                if action in counts:
                    counts[action] += 1

                binned_act = '%s_%d' % (action, bin_size)
                if counts[action] == bin_size:
                # case1: found sub sequence of right length to replace
                    binned_seq.append(binned_act)
                    counts[action] = 0
                elif i+1 < seq_len and self.sequence[i+1] != action:
                # case2: sub sequence is too short (different next action)
                # count subsequense as individual actions, not as bin.
                    binned_seq += [action] * counts[action]
                    counts[action] = 0
                elif i+1 == seq_len:
                # case3: sqb sequence is too short, but last one in seq
                # count subsequense as individual actions, not as bin.
                    binned_seq += [action] * counts[action]
                    counts[action] = 0
                else:
                # case4: count but don't add, will happen in next iter
                    pass
            else: # no binning for this action
                binned_seq.append(action)
        return binned_seq


def main():
    """create sequence file as input for iseekviz"""
    import argparse
    aparser = argparse.ArgumentParser()
    aparser.add_argument(
        'file',
        help="""Specify input file, format is comma separated actions, one user
        sequence per line.""")
    aparser.add_argument(
        '-w',
        '--whitelistfile',
        help="""Specify selected actions file, format is one action per line.
        These actions are included in the output.""",
        default='selected_actions.txt')
    aparser.add_argument(
        '-b',
        '--blacklistfile',
        help="""Specify all actions file, format is one action per line.  The
        set difference of these actions and the whitelist actions are excluded
        from the output.""",
        default='all_actions.txt')
    args = aparser.parse_args()

    whitelist = []
    with open(args.whitelistfile, 'r') as fhdl:
        whitelist = fhdl.read().split('\n')
    blacklist = []
    with open(args.blacklistfile, 'r') as fhdl:
        blacklist = set(fhdl.read().split('\n')) - set(whitelist)
    with open(args.file, 'r') as fhdl:
        for line in fhdl.xreadlines():
            actseq = ActionSequence(line.strip())
            binseq = actseq.bin_repetitions(5, whitelist, blacklist)
            if len(binseq) > 100 and len(binseq) < 300:
                print ','.join(binseq)


if __name__ == '__main__':
    main()
