#
#  iseekviz: a visualization tool for search logs by Marc Bron, 2012
#
# contributions by Hendrike Peetz
# debugging and beta testing by Jiyin He
#
#
import random
import argparse
import logging
import doctest
from colors import COLORS
logging.basicConfig(format='[%(levelname)s] (%(threadName)-10s) %(asctime)s %(message)s',level=logging.DEBUG)

NODEPARAMS={'label':'',
'fixedsize':'true',
'width':'1',
'height':'1',
'style':'filled',
'penwidth':'25'}

EDGEPARAMS={'len':'1',
'style':'invis'}

BARPARAMS={'fixedsize':'true',
'width':'10',
'height':'1'}

# String -> ListOfStrings
def read_file(fn):
	"""	Read file named fn, with sessions of search patterns one on
	each line and comma seperated. Output list of strings, each
	representing a session.
	
	>>> read_file("test/test_wcount.txt")
	['1 q0,0,0,0', '1 q0,0,0,0,0', '3 q0,q0,q1,17', '3 q0,q1,17,19']

	>>> read_file("test/test.txt")
	['q0,0,0,0', 'q0,0,0,0,0', 'q0,q0,q1,17', 'q0,q1,17,19']
	"""
	fh = open(fn,'r')
	text = fh.read()
	fh.close()
	text = text.rstrip("\n") # remove any extra newline
	lines = text.split("\n")
	return lines

# ListOfStrings -> ListOfCounts, ListOfStrings
def separate_session_counts(sessions):
	"""If we have sessions with counts, extract the counts, and output
	a list of counts and the session strings without counts.

	>>> separate_session_counts(['1 q0,0,0,0', '1 q0,0,0,0,0',\
									'3 q0,q0,q1,17', '3 q0,q1,17,19'])
	(['1', '1', '3', '3'], ['q0,0,0,0', 'q0,0,0,0,0',\
 'q0,q0,q1,17', 'q0,q1,17,19'])

	"""
	counts=[]
	filtered_sessions = []
	for l in sessions:
		count,session = l.split(" ",2)
		counts.append(count)
		filtered_sessions.append(session)
	return counts, filtered_sessions

# String -> ListOfEdges
def make_edges(session):
	""" consume a string representing a session, produce edges
	datastruct. Edge datastruct: tuple with (tail, head, params)
	params contain user defined drawing instuctions, e.g., color.

	>>> make_edges('q0,q0,q1,17')
	[('q0', 'q0', {}), ('q0', 'q1', {}), ('q1', '17', {})]

	>>> make_edges(' q0, q1 , 17 ,19 ')
	[('q0', 'q1', {}), ('q1', '17', {}), ('17', '19', {})]
	"""
	actions = session.split(",")
	if len(actions) < 2:
		print "not enough actions to create edges"
		return []
	edges = []
	edge = [actions[0].strip()]
	for a in actions[1:]:
		a = a.strip()
		edge.append(a)
		edge.append({})
		edges.append(tuple(edge))
		edge = [a]
	return edges

# String -> ListOfNodes
def make_nodes(session):
	""" consume a string representing a session, produce nodes
	datastruct. Node datastruct: tuple with (node, params)
	params contain user defined drawing instuctions, e.g., color.

	>>> make_nodes('q0,q0,q1,17')
	[('q0', {}), ('q0', {}), ('q1', {}), ('17', {})]

	>>> make_nodes(' q0, q1 , 17 ,19 ')
	[('q0', {}), ('q1', {}), ('17', {}), ('19', {})]
	"""
	actions = session.split(",")
	nodes = []
	for a in actions:
		a = a.strip()
		nodes.append((a, {}))
	return nodes

# ListOfNodes -> DictOfColors
def make_color_table(nodes):
	doctest.SKIP
	"""
	>>> make_color_table([('q0', {}), ('q1', {}), ('17', {}),\
							('19', {})])
	{'q1': 'oldlace', 'q0': 'red', '17': 'violet', '19': 'seagreen'}

	>>> make_color_table([('q0', {}), ('q0', {}), ('q0', {})])
	{'q0': 'oldlace'}

	"""
	colord = {}
	callstring = ""
	# only assign colors to a node once
	node_names = list(set([n[0] for n in nodes]))
	num_nodes = len(node_names)
	# sample without replacement
	colors = random.sample(COLORS, num_nodes)
	for i in range(num_nodes):
		color = colors[i]
		n_name = node_names[i]
		colord[n_name] = color
		callstring+= " %s:%s"%(n_name, color)
	logging.info("Selecting random colors, \
					for the same colors, use\n %s"%callstring)
	return colord

# ListOfNodes, DictOfColors, DictOfParams -> ListOfNodes
def add_node_drawing_params(nodes, color_table, params):	
	""" consume nodes datastruct, color table, and general drawing
	parameters. Adds color and drawing parameters to nodes.
	>>> add_node_drawing_params([('q0', {}), ('q1', {}),\
					('17', {}), ('19', {})],\
					{'q0':'red','q1':'blue','17':'yellow','19':'green'},\
					{"label":"","fixedsize":"true","width":"1",\
					"height":"1","style":"filled","penwidth":"25"})
	[('q0', {'style': 'filled', 'width': '1', 'color': 'red', 'height': '1', 'fixedsize': 'true', 'fillcolor': 'red', 'label': '', 'penwidth': '25'}),\
 ('q1', {'style': 'filled', 'width': '1', 'color': 'blue', 'height': '1', 'fixedsize': 'true', 'fillcolor': 'blue', 'label': '', 'penwidth': '25'}),\
 ('17', {'style': 'filled', 'width': '1', 'color': 'yellow', 'height': '1', 'fixedsize': 'true', 'fillcolor': 'yellow', 'label': '', 'penwidth': '25'}),\
 ('19', {'style': 'filled', 'width': '1', 'color': 'green', 'height': '1', 'fixedsize': 'true', 'fillcolor': 'green', 'label': '', 'penwidth': '25'})]"""
	for i in range(len(nodes)):
		n_name = nodes[i][0]
		n_params = nodes[i][1]
		n_params["color"] = color_table[n_name]
		n_params["fillcolor"] = color_table[n_name]
		for (k,v) in params.items():
			n_params[k] = v
	return nodes

# ListOfEdges, DictOfParams -> ListOfEdges
def add_edge_drawing_params(edges, params):
	"""consume edge datastruct and edge drawing parameters, e.g.,
	whether or not edges are invisible. Add drawing parameters to
	the edgelist

	>>> add_edge_drawing_params([('q0', 'q0', {}), ('q0', 'q1', {}),\
			('q1', '17', {})], {'len':'1', 'style':'invis'})
	[('q0', 'q0', {'style': 'invis', 'len': '1'}),\
 ('q0', 'q1', {'style': 'invis', 'len': '1'}),\
 ('q1', '17', {'style': 'invis', 'len': '1'})]
	"""
	for e in edges:
		for (k,v) in params.items():
			e[2][k]=v
	return edges

# ListOfNodes, Integer, String -> String,String
def make_count_bar(nodes,cluster,count):
	""" consume nodes, Integer, String. Produce a node representing the
	frequency with which the pattern is observed. Produce an edge
	connecting this node to the first node in the session. Parameters in DOT
	format. Integer represents the cluster ID, so this node will have a
	unique ID.

	>>> make_count_bar([('q0', {})],0,'10')
	('"count_0" [label="{||||||||||}",fixedsize=true, width=20, height=1];\\n', '"count_0" -> "0_0_q0" [style=invis, len=1];\\n')
	"""

	barnode = '"count_%i" [label="{%s}",'%(cluster,"|"*int(count))
	buf = []
	for (k,v) in BARPARAMS.items():
		buf.append(k+"="+v)
	barnode +=', '.join(buf)+"];\n"

	startnode = "%i_0_%s"%(cluster,nodes[0][0])
	baredge = '"count_%i" -> "%s" ['%(cluster,startnode)	
	buf= []
	for (k,v) in EDGEPARAMS.items():
		buf.append(k+"="+v)
	baredge += ', '.join(buf)+'];\n'

	return barnode,baredge

# ListOfNodes, Integer -> ListOfStrings
def nodes_to_dot_format(nodes,cluster):
	""" consume nodes and Integer. Produce a list of strings each
	representing a node with parameters in DOT format. Integer
	represents the cluster ID, so each node will have a unique ID.

	>>> nodes_to_dot_format([('q0', {'color': 'red', 'label': ''})],0)
	['"0_0_q0" [color=red, label=""];']

	>>> nodes_to_dot_format([('q0', {'style': 'filled', 'width': '1', 'color':\
 'red', 'height': '1', 'fixedsize': 'true', 'fillcolor': 'red',\
 'label': '', 'penwidth': '25'}),\
 ('q1', {'style': 'filled', 'width': '1', 'color': 'blue', 'height':\
 '1', 'fixedsize': 'true', 'fillcolor': 'blue', 'label': '',\
 'penwidth': '25'}),\
 ('17', {'style': 'filled', 'width': '1', 'color': 'yellow',\
 'height': '1', 'fixedsize': 'true', 'fillcolor': 'yellow',\
 'label': '', 'penwidth': '25'}),\
 ('19', {'style': 'filled', 'width': '1', 'color': 'green',\
 'height': '1', 'fixedsize': 'true', 'fillcolor': 'green',\
 'label': '', 'penwidth': '25'})], 0)
	['"0_0_q0" [penwidth=25, width=1, style=filled, fillcolor=red,\
 color=red, fixedsize=true, label="", height=1];', '"0_1_q1"\
 [penwidth=25, width=1, style=filled, fillcolor=blue, color=blue,\
 fixedsize=true, label="", height=1];', '"0_2_17" [penwidth=25,\
 width=1, style=filled, fillcolor=yellow, color=yellow, fixedsize=true,\
 label="", height=1];', '"0_3_19" [penwidth=25, width=1, style=filled,\
 fillcolor=green, color=green, fixedsize=true, label="", height=1];']

	"""
	dot_nodes=[]
	i=0
	for n in nodes:
		buffer = []
		params = n[-1]
		for (k,v) in params.items():
			if k == "label":
				buffer.append( k+'="'+v+'"' )
			else:
				buffer.append( k+'='+v )
		dot_node = '"%i_%i_%s"'%(cluster,i,n[0])
		dot_node += ' [' + ", ".join(buffer) + "];"
		dot_nodes.append(dot_node)
		i+=1	
	return dot_nodes


# ListOfEdges -> ListOfStrings
def edges_to_dot_format(edges, clusterID):
	"""produce edges in dot format

	>>> edges_to_dot_format([('q0', 'q1', {'style': 'invis', 'len': '1'})],0)
	['"0_0_q0" -> "0_1_q1" [style=invis, len=1];']

	"""
	dot_edges=[]
	prev = 0
	cur = 1
	for e in edges:
		buffer = []
		params = e[-1]
		for (k,v) in params.items():
			buffer.append( k+'='+v )
		dot_edge = '"%i_%i_'%(clusterID,prev)+e[0]
		dot_edge += '" -> "%i_%i_'%(clusterID,cur)+e[1]
		dot_edge += '" [' + ", ".join(buffer) + "];"
		dot_edges.append(dot_edge)
		prev = cur
		cur +=1
	return dot_edges

# ListOfStrings, ListOfStrings, DictOfParams -> String
def make_subgraph(nodes,edges,params,clusterID,counts=[]):
	doctest.NORMALIZE_WHITESPACE
	""" consume nodes datastruct, edge datastuct, subgraph params,
	produce string representing a subgraph
	
	>>> make_subgraph(	[('q0', {'color': 'red', 'label': ''}),\
				('q0', {'color': 'red', 'label': ''})],\
				[('q0', 'q0', {'style': 'invis', 'len': '1'})],\
				{'rankdir':'LR','K':'0','node':'[shape = record];'},\
				0)
	'subgraph "cluster_0" {\n\
		rankdir=LR;\n\
		K=0;\n\
		node [shape = record];\n\
\n\
		"0_0_q0" [color:red, label:""];\n\
		"0_1_q0" [color:red, label:""];\n\
\n\
		"0_0_q0" -> "0_1_q0" [style:invis, len:1];\n\
	}\n'

	>>> make_subgraph(	[('q0', {'color': 'red', 'label': ''}),\
				('q0', {'color': 'red', 'label': ''})],\
				[('q0', 'q0', {'style': 'invis', 'len': '1'})],\
				{'rankdir':'LR','K':'0','node':'[shape = record];'},\
				0,[10])
	'subgraph "cluster_0" {\n\
		rankdir=LR;\n\
		K=0;\n\
		node [shape = record];\n\
\n\
		"count_0" [label="{||||||||||}"];\n\
		"0_0_q0" [color:red, label:""];\n\
		"0_1_q0" [color:red, label:""];\n\
\n\
		"0_0_q0" -> "0_1_q0" [style:invis, len:1];\n\
		"count_0" -> "0_0_q0" [style:invis, len:1];\n\
	}\n'
	"""

	subgraph = """subgraph "cluster_%s" {\n"""%(clusterID)
	for (k,v) in params.items():
		subgraph += "\t\t"+k+" = "+v+"\n"

	if counts:
		barnode,baredge = make_count_bar(nodes,clusterID,counts[clusterID])
		subgraph += "\t\t" + barnode + '\n'

	dot_nodes = nodes_to_dot_format(nodes,clusterID)
	for n in dot_nodes:
		subgraph += "\t\t"+ n + '\n'

	dot_edges = edges_to_dot_format(edges,clusterID)
	for e in dot_edges:
		subgraph += "\t\t"+ e + '\n'

	if counts:
		barnode,baredge = make_count_bar(nodes,clusterID,counts[clusterID])
		subgraph += "\t\t" + baredge + '\n'
		 	
	subgraph += '}\n\n'
	
	return subgraph

# String -> String
def make_dot_graph(lines,colortable,counts):
	doctest.SKIP
	""" consume a filename and output a graph to be consumed by DOT

	>>> make_dot_graph("test/test.txt")
	[]

	"""
	graph = 'digraph G{\n\
		rankdir=LR;\n\
		graph [];\n\
		node [shape = record];\n\
		K=0;\n\n'

	clusterID = 0
	subgraphs = []

	params = {'rankdir':'LR;',
			'K':'0;',
			'node [shape':'record];'}
	for line in lines:
		nodes = make_nodes(line)
		edges = make_edges(line)
		nodes = add_node_drawing_params(nodes,colortable,NODEPARAMS)
		edges = add_edge_drawing_params(edges,EDGEPARAMS)
		subgraphs.append(make_subgraph(nodes,edges,params,clusterID,counts))
		clusterID+=1

	for subgraph in subgraphs:
		graph+= subgraph
	graph += '}'
	return graph
		

def main():
	parser = argparse.ArgumentParser(description=u'ISeekViz: a cluster visualisation of . (c) 2013 Marc Bron <m.bron@uva.nl> and Maria-Hendrike Peetz <m.h.peetz@uva.nl> .')
	parser.add_argument('-f', metavar="FILE", required=True, 
		help='An input file with one session per line, tab seperated')

	parser.add_argument('-F', metavar="FILE", required=True, 
		help='An output file with one session per line, tab seperated')
	
	parser.add_argument('-m', nargs='*', help='Mapping terms to dot\
			 conform colors. Terms should not contain whitespace or\
			 ":": use -m term:colour')

	parser.add_argument('-d', action='store_true', default=False,
		help='If session are preceded by a count indicating\
			the frequency of the session, i.e., "56 q0,q1,q3,..."\
			Default is no.')

	parser.add_argument('-c', action='store_true', default=False,
		help='If clustering should be done or not. Default is no.')

	args = parser.parse_args()

# TODO clustering	
#	if args.c:
#		mapping = {}
#		for i,t in enumerate(types):
#			mapping[t] = str(i)
#		order = cluster(d, mapping)
	filename = args.f
	lines = read_file(filename)

	counts = []
	if args.d:
		counts,lines=separate_session_counts(lines)	

	# get all unique actions (nodes)
	nodes = []
	for line in lines:
		nodes += make_nodes(line)
	node_names = list(set([node[0] for node in nodes]))

	colortable={}
	if args.m:
		logging.info("Using custom colors.")
		for node_color_pair in args.m:
			node,color = node_color_pair.split(":")
			colortable[node] = color
		# make colors not mentioned on input grey
		for node in node_names:
			if not node in colortable:
				colortable[node] = "grey"
				logging.info("Greying out:"+node)
	else: # use random colors
		colortable = make_color_table(nodes)

	graph = make_dot_graph(lines,colortable,counts)
	
	outfilename = args.F
	fh =  open(outfilename,'w')
	fh.write(graph)
	fh.close()
	
if __name__ == '__main__':
#	doctest.testmod()
	main()

